class UsersController < ApplicationController
  require 'rest-client'
  require 'json'

  def login
  end

  def logout
    api_request 'delete', nil
    session[:user] = nil
    redirect_to users_login_path
  end

  def auth
    request = api_request 'post', api_user: user_params.to_h
    session[:user] = ActiveModelSerializers::Deserialization.jsonapi_parse(request)
    redirect_to root_path
  end



  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def api_request(action, payload)
    url = '{{url}}'
    headers = {}
    headers['content_type'] = :json
    headers['accept'] = :json
    unless session[:user].nil?
      headers['X-USER-TOKEN'] = session[:user]['token']
      headers['X-USER-EMAIL'] = session[:user]['key']
    end
    case action
    when 'post'
      response = RestClient.post url, payload.to_json, content_type: :json, headers: headers
    when 'delete'
      response = RestClient.delete url, {content_type: :json, 'X-USER-TOKEN': session[:user]['token'], 'X-USER-EMAIL': session[:user]['key'] }
    end
    if response.body != ""
      JSON.parse response
    end
  end
end
