class CarersController < ApplicationController

  def index
    Carer.with_headers(credientials) do
      @carers = Carer.all
    end
  end
end
