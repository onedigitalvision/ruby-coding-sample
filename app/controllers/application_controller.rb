class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def user_authenitcate
    redirect_to users_login_path if session[:user].nil?
  end

  def credientials
    headers = {}
    headers['X-USER-TOKEN'] = session[:user]['token']
    headers['X-USER-EMAIL'] = session[:user]['key']
    headers
  end
end
