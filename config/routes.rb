Rails.application.routes.draw do
  resources :carers
  root to: 'home#index'
  get 'users/login', controller: 'users#login'
  post 'users/auth', controller: 'users#auth'
  post 'users/logout', controller: 'users#logout'
end
