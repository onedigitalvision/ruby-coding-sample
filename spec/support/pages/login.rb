class LoginPage < SitePrism::Page
  set_url '/users/login'

  element :email_field, "input[name='user[email]']"
  element :password_field, "input[name='user[password]']"
  element :login_button, 'button[type=submit]'
end
