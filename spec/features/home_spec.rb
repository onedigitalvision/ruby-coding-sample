require 'rails_helper'

RSpec.feature 'Home Page', type: :feature do
  context 'As a guest' do
    describe 'visit the home page' do
      before :all do
        # support/pages/home.rb
        @home = HomePage.new
      end

      before :each do
        @home.load
      end
      it 'should redirect to the login user page' do
        expect(current_path).to eq users_login_path
      end
    end
  end
end
