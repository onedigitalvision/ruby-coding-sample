require 'rails_helper'

RSpec.feature 'LoginUsers', type: :feature do
  context 'As a user I want to login' do
    before :all do
      # support/pages/login.rb
      @login = LoginPage.new
    end

    before :each do
      @login.load
    end
    describe 'visit login page' do
      it 'should display the login page' do
        expect(@login).to be_displayed
      end

      it 'should display email and password fields and a submit button' do
        expect(@login).to have_email_field
        expect(@login).to have_password_field
        expect(@login).to have_login_button
      end
    end

    describe 'logging in with valid details' do
      it 'should allow me to login' do
        # Needs stubbing but running out of time
        @login.email_field.set 'test.operator@vida.co.uk'
        @login.password_field.set 'VidaTest'
        @login.login_button.click
        expect(current_path).to eq(root_path)
        expect(page).to have_content 'View Carers'
      end
    end
  end
end
