FactoryGirl.define do
  factory :user do
    key { FFaker::Internet.email }
    token '1234456789'
  end
end
