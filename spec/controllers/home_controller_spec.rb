require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  context 'As a guest' do
    describe 'GET #index' do
      subject { get :index }
      before { subject }

      it { is_expected.to redirect_to users_login_path }
    end
  end
  context 'As a logged in user' do
    describe 'GET #index' do
      let!(:session) { { user: { token: '1234', key: 'a@a.com' } } }
      subject { get :index, session: session }
      before { subject }

      it { is_expected.to render_template :index}
    end
  end
end
